package main

import (
	"fmt"
	"runtime"

	"./gmp"
	"./sty"
	"gitlab.com/tapir/sfml/v2.3/sfml"
	"gitlab.com/tapir/utime"
)

func init() {
	runtime.LockOSThread()
}

func main() {
	var (
		window  = sfml.NewRenderWindow(sfml.VideoMode{1280, 720, 32}, "Test", sfml.StyleDefault, nil)
		texture = sfml.NewRenderTexture(1280, 744, false)
		speed   = float32(15000)
		camera  = sfml.Vector2f{}
		font    = sfml.NewFont("assets/orkney.otf")
		frame   = sfml.NewRectangleShape(sfml.Vector2f{64, 64})
		coord   = sfml.NewText("", font, 12)
		debug   = false
	)

	coord.SetColor(sfml.ColorYellow)
	frame.SetOutlineThickness(1)
	frame.SetOutlineColor(sfml.ColorRed)
	frame.SetFillColor(sfml.ColorTransparent)

	// Read bil level files
	s := sty.ReadGTAStyle("assets/bil.sty")
	m := gmp.ReadGTAMap("assets/bil.gmp")

	// Get tile texture
	tileData := s.GetTileAtlas()

	// Get map blocks
	blocks := m.GetBlock()

	//Time from utime
	timePrevious := utime.Now()

	for window.IsOpen() {
		dt := float32(utime.Now() - timePrevious)
		timePrevious = utime.Now()

		if event := window.PollEvent(); event != nil {
			switch event.Type {
			case sfml.EventClosed:
				window.Close()
			case sfml.EventKeyPressed:
				if event.Key.Code == sfml.KeyUp {
					camera.Y -= speed * dt
				}
				if event.Key.Code == sfml.KeyDown {
					camera.Y += speed * dt
				}
				if event.Key.Code == sfml.KeyLeft {
					camera.X -= speed * dt
				}
				if event.Key.Code == sfml.KeyRight {
					camera.X += speed * dt
				}
				if event.Key.Code == sfml.KeyF1 {
					debug = !debug
				}
			}
		}

		// Extremities of the map
		if camera.X < 0 {
			camera.X = 0
		}
		if camera.Y < 0 {
			camera.Y = 0
		}
		if camera.X > 15104 {
			camera.X = 15104
		}
		if camera.Y > 15564 {
			camera.Y = 15564
		}

		// Find tile number
		tx := int(camera.X / 64)
		ty := int(camera.Y / 64)

		// Render tiles
		texture.Clear(sfml.ColorBlack)
		for k := 0; k < 8; k++ {
			for j := 0; j < 13; j++ {
				for i := 0; i < 21; i++ {
					var (
						block                = blocks[k][i+tx][j+ty]
						ground, _            = block.DecodeSlope()
						lid, _, _, flip, rot = block.DecodeLidTile()
					)

					if ground != 0 && lid < 992 {
						tilePos := sfml.Vector2f{float32(i)*64 + 32, float32(j)*64 + 32}
						tileSpr := sfml.NewSpriteFromRect(tileData.Texture, tileData.Tiles[lid])
						tileSpr.SetOrigin(sfml.Vector2f{32, 32})
						tileSpr.SetPosition(tilePos)

						switch rot {
						case 0:
							tileSpr.SetRotation(0)
						case 1:
							// Special case
							if flip {
								tileSpr.SetRotation(270)
							} else {
								tileSpr.SetRotation(90)
							}
						case 2:
							tileSpr.SetRotation(180)
						case 3:
							// Special casse
							if flip {
								tileSpr.SetRotation(90)
							} else {
								tileSpr.SetRotation(270)
							}
						}

						// Mirror tile
						if flip {
							tileSpr.Scale(sfml.Vector2f{-1, 1})
						}

						texture.Draw(tileSpr)

						// Debug information
						if debug {
							debpos := sfml.Vector2f{float32(i) * 64, float32(j) * 64}
							coord.SetPosition(debpos)
							frame.SetPosition(debpos)
							coord.SetString(fmt.Sprintf("(%v, %v)", i+tx, j+ty))
							texture.Draw(frame)
							texture.Draw(coord)
						}
					}
				}
			}
		}

		texture.Display()
		window.Draw(sfml.NewSprite(texture.GetTexture()))
		window.Display()
	}
}
