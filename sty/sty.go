package sty

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/tapir/sfml/v2.3/sfml"
)

const DefaultPalette = -1

type GTAStyle struct {
	Type    string
	Version int
	Chunks  map[string]Chunk
}

type Chunk struct {
	Name string
	Size int
	Data interface{}
}

type PaletteIndex [16384]uint16

type PhysicalPalette [][65536]uint8

type PaletteBase struct {
	Tile         uint16
	Sprite       uint16
	CarRemap     uint16
	PedRemap     uint16
	CodeObjRemap uint16
	MapObjRemap  uint16
	UserRemap    uint16
	FontRemap    uint16
}

type Tile [][256][256]uint8

type SpriteGraphics []uint8

type SpriteIndex []SpriteEntry

type SpriteEntry struct {
	Ptr uint32
	W   uint8
	H   uint8
	_   uint16
}

type SpriteBase struct {
	Car     uint16
	Ped     uint16
	CodeObj uint16
	MapObj  uint16
	User    uint16
	Font    uint16
}

type DeltaEntry struct {
	Sprite     uint16
	DeltaCount uint8
	DeltaSize  []uint16
}

type DeltaIndex []DeltaEntry

type delta struct {
	Pos    uint16
	Length uint8
	Data   []uint8
}

type DeltaStore []uint8

type ObjectInfo struct {
	Model  uint8
	Sprite uint8
}

type MapObjects []ObjectInfo

type DoorInfo struct {
	Rx int8
	Ry int8
}

type CarInfo struct {
	Model             uint8
	Sprite            uint8
	W                 uint8
	H                 uint8
	NumRemaps         uint8
	Passengers        uint8
	Wreck             uint8
	Rating            uint8
	FrontWheelOffset  int8
	RearWheelOffset   int8
	FrontWindowOffset int8
	RearWindowOffset  int8
	InfoFlags         uint8
	InfoFlags2        uint8
	Remap             []uint8
	NumDoors          uint8
	Doors             []DoorInfo
}

type Cars []CarInfo

type TileAtlas struct {
	Texture *sfml.Texture
	Tiles   []sfml.Recti
}

// Get physical palette
func (g *GTAStyle) GetPalette(palId int) []sfml.Color {
	var (
		p       = g.Chunks["PPAL"].Data.(PhysicalPalette)
		page    = palId / 64
		x       = (palId % 64) * 4
		palette []sfml.Color
		color   sfml.Color
	)

	for y := 0; y < 256; y++ {
		palId := y*256 + x

		// Convert BGRA to RGBA
		color.R = p[page][palId+2]
		color.G = p[page][palId+1]
		color.B = p[page][palId]
		color.A = 0xFF

		palette = append(palette, color)
	}

	// First element of every palette is transparent
	palette[0].A = 0x00

	return palette
}

// Get all physical palettes
func (g *GTAStyle) GetAllPalettes() [][]sfml.Color {
	var (
		p          = g.Chunks["PPAL"].Data.(PhysicalPalette)
		paletteNum = len(p) * 64
		palettes   [][]sfml.Color
	)

	for i := 0; i < paletteNum; i++ {
		palettes = append(palettes, g.GetPalette(i))
	}

	return palettes
}

// Get tile
func (g *GTAStyle) GetTile(tileId int) *sfml.Image {
	var (
		vpalette = g.Chunks["PALX"].Data.(PaletteIndex)
		tiles    = g.Chunks["TILE"].Data.(Tile)
		image    = sfml.NewEmptyImage(64, 64)
		page     = tileId / 16
		tileNum  = tileId % 16
		pageData = tiles[page]
		ystart   = (tileNum / 4) * 64
		xstart   = (tileNum % 4) * 64
	)

	palette := g.GetPalette(int(vpalette[tileId]))

	for y := ystart; y < ystart+64; y++ {
		for x := xstart; x < xstart+64; x++ {
			image.SetPixel(uint(x%64), uint(y%64), palette[pageData[y][x]])
		}
	}

	return image
}

// Get all tiles
func (g *GTAStyle) GetAllTiles() []*sfml.Image {
	var (
		tilesData  = g.Chunks["TILE"].Data.(Tile)
		totalTiles = len(tilesData) * 16
		tiles      []*sfml.Image
	)

	for i := 0; i < totalTiles; i++ {
		tiles = append(tiles, g.GetTile(i))
	}

	return tiles
}

// Get sprite with virtual palette index
func (g *GTAStyle) GetSprite(spriteId, vpalId int) *sfml.Image {
	var (
		vpalette    = g.Chunks["PALX"].Data.(PaletteIndex)
		spriteData  = g.Chunks["SPRG"].Data.(SpriteGraphics)
		spriteIndex = g.Chunks["SPRX"].Data.(SpriteIndex)
		paletteBase = g.Chunks["PALB"].Data.(PaletteBase)
		ptr         = int(spriteIndex[spriteId].Ptr)
		width       = int(spriteIndex[spriteId].W)
		height      = int(spriteIndex[spriteId].H)
		image       = sfml.NewEmptyImage(uint(width), uint(height))
		basex       = ptr % 256
		basey       = ptr / 256
		base        = ptr - basex - basey*256
	)

	if vpalId == DefaultPalette {
		vpalId = spriteId + int(paletteBase.Tile)
	}

	palette := g.GetPalette(int(vpalette[vpalId]))

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			spriteId = base + ((basex + x) + (basey+y)*256)
			pixel := palette[spriteData[spriteId]]
			image.SetPixel(uint(x), uint(y), pixel)
		}
	}

	return image
}

// Get all sprites with default palette
func (g *GTAStyle) GetAllSprites() []*sfml.Image {
	var (
		spriteNum = len(g.Chunks["SPRX"].Data.(SpriteIndex))
		sprites   []*sfml.Image
	)

	for i := 0; i < spriteNum; i++ {
		sprites = append(sprites, g.GetSprite(i, DefaultPalette))
	}

	return sprites
}

// Get a delta for a sprite
func (g *GTAStyle) GetDelta(spriteId, deltaId int) *sfml.Image {
	var (
		offset       = 0
		dataLen      = 0
		recordLen    = 0
		pos          = 0
		deltaIndexes = g.Chunks["DELX"].Data.(DeltaIndex)
		deltaStore   = g.Chunks["DELS"].Data.(DeltaStore)
		spriteIndex  = g.Chunks["SPRX"].Data.(SpriteIndex)
		paletteBase  = g.Chunks["PALB"].Data.(PaletteBase)
		vpalette     = g.Chunks["PALX"].Data.(PaletteIndex)
		width        = int(spriteIndex[spriteId].W)
		height       = int(spriteIndex[spriteId].H)
		image        = sfml.NewEmptyImageFromColor(uint(width), uint(height), sfml.ColorTransparent)
		vpalId       = spriteId + int(paletteBase.Tile)
		palette      = g.GetPalette(int(vpalette[vpalId]))
		minideltas   []delta
	)

	for _, d := range deltaIndexes {
		if int(d.Sprite) == spriteId {
			for i := 0; i < deltaId; i++ {
				offset += int(d.DeltaSize[i])
			}
			dataLen = offset + int(d.DeltaSize[deltaId])
			break
		} else {
			for _, o := range d.DeltaSize {
				offset += int(o)
			}
		}
	}

	buffer := bytes.NewBuffer(deltaStore[offset:dataLen])
	for buffer.Len() > 0 {
		var minidelta delta
		binary.Read(buffer, binary.LittleEndian, &minidelta.Pos)
		binary.Read(buffer, binary.LittleEndian, &minidelta.Length)
		minidelta.Data = make([]uint8, minidelta.Length)
		binary.Read(buffer, binary.LittleEndian, &minidelta.Data)
		minideltas = append(minideltas, minidelta)
	}

	for _, minidelta := range minideltas {
		pos += int(minidelta.Pos) + recordLen
		recordLen = int(minidelta.Length)
		x := pos % 256
		y := pos / 256
		for i, v := range minidelta.Data {
			image.SetPixel(uint(x+i), uint(y), palette[v])
		}
	}

	return image
}

// Get all deltas for a given sprite
func (g *GTAStyle) GetAllDeltas(spriteId int) []*sfml.Image {
	var (
		deltaIndexes = g.Chunks["DELX"].Data.(DeltaIndex)
		deltas       []*sfml.Image
	)

	for _, d := range deltaIndexes {
		if int(d.Sprite) == spriteId {
			for i := 0; i < int(d.DeltaCount); i++ {
				deltas = append(deltas, g.GetDelta(spriteId, i))
			}
			break
		}
	}

	return deltas
}

func (g *GTAStyle) GetTileAtlas() TileAtlas {
	var (
		tiles = g.GetAllTiles()
		atlas = sfml.NewEmptyImageFromColor(2048, 2048, sfml.ColorTransparent)
		grid  TileAtlas
	)

	for i, t := range tiles {
		dstx := (i % 32) * 64
		dsty := (i / 32) * 64
		for srcy := 0; srcy < 64; srcy++ {
			for srcx := 0; srcx < 64; srcx++ {
				atlas.SetPixel(uint(dstx+srcx), uint(dsty+srcy), t.GetPixel(uint(srcx), uint(srcy)))
			}
		}
		grid.Tiles = append(grid.Tiles, sfml.Recti{dstx, dsty, 64, 64})
	}

	grid.Texture = sfml.NewTextureFromImage(atlas)

	return grid
}

func ReadGTAStyle(filename string) *GTAStyle {
	var (
		newSTY   = new(GTAStyle)
		fileType [4]byte
		version  uint16
	)

	newSTY.Chunks = make(map[string]Chunk)

	// Read whole file to the buffer
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("Can't open file: ", filename)
	}

	// Read file info
	buffer := bytes.NewBuffer(file)
	binary.Read(buffer, binary.LittleEndian, &fileType)
	binary.Read(buffer, binary.LittleEndian, &version)
	if string(fileType[:]) != "GBST" {
		log.Fatal("Illegal style file type: ", string(fileType[:]))
	}

	newSTY.Type = string(fileType[:])
	newSTY.Version = int(version)

	// Read chunks
	for buffer.Len() > 0 {
		var (
			newChunk  Chunk
			chunkName [4]byte
			chunkSize uint32
		)

		// Read chunk name
		binary.Read(buffer, binary.LittleEndian, &chunkName)

		// Read chunk size
		binary.Read(buffer, binary.LittleEndian, &chunkSize)

		newChunk.Name = string(chunkName[:])
		newChunk.Size = int(chunkSize)

		// Decode chunk types
		switch newChunk.Name {
		case "PALX": //
			// Palette index is a virtual palette to physical palette conversion table
			var palx PaletteIndex
			binary.Read(buffer, binary.LittleEndian, &palx)
			newChunk.Data = palx

		case "PPAL":
			// Physical palettes hold the real BGRA color values
			// There is no real alpha. It's set to 0xFF by default
			pageNum := newChunk.Size / 65536
			ppal := make(PhysicalPalette, pageNum)
			binary.Read(buffer, binary.LittleEndian, &ppal)
			newChunk.Data = ppal

		case "PALB": //
			// Palette base is offset values for palettes
			var palb PaletteBase
			binary.Read(buffer, binary.LittleEndian, &palb)
			newChunk.Data = palb

		case "TILE":
			// Tile pixel data
			pageNum := newChunk.Size / 65536
			tile := make(Tile, pageNum)
			binary.Read(buffer, binary.LittleEndian, &tile)
			newChunk.Data = tile

		case "SPRG": //
			// Sprite pixel data
			sprg := make(SpriteGraphics, newChunk.Size)
			binary.Read(buffer, binary.LittleEndian, &sprg)
			newChunk.Data = sprg

		case "SPRX": //
			// Sprite properties like the width and height
			spriteNum := newChunk.Size / 8
			sprx := make(SpriteIndex, spriteNum)
			binary.Read(buffer, binary.LittleEndian, &sprx)
			newChunk.Data = sprx

		case "SPRB":
			// Sprite base is offset values for sprites
			var sprb SpriteBase
			binary.Read(buffer, binary.LittleEndian, &sprb)
			newChunk.Data = sprb

		case "DELX":
			// Delta index
			var delx DeltaIndex
			tempBuffer := bytes.NewBuffer(buffer.Next(newChunk.Size))
			for tempBuffer.Len() > 0 {
				var dele DeltaEntry
				binary.Read(tempBuffer, binary.LittleEndian, &dele.Sprite)
				binary.Read(tempBuffer, binary.LittleEndian, &dele.DeltaCount)
				tempBuffer.Next(1) // Padding
				dele.DeltaSize = make([]uint16, dele.DeltaCount)
				binary.Read(tempBuffer, binary.LittleEndian, &dele.DeltaSize)
				delx = append(delx, dele)
			}
			newChunk.Data = delx

		case "DELS":
			// Delta store holds deltas pixel values
			dels := make(DeltaStore, newChunk.Size)
			binary.Read(buffer, binary.LittleEndian, &dels)
			newChunk.Data = dels

		case "OBJI":
			// Map object information
			objNum := newChunk.Size / 2
			obji := make(MapObjects, objNum)
			binary.Read(buffer, binary.LittleEndian, &obji)
			newChunk.Data = obji

		case "CARI":
			// Car information
			var cars Cars
			tempBuffer := bytes.NewBuffer(buffer.Next(newChunk.Size))
			for tempBuffer.Len() > 0 {
				var info CarInfo
				binary.Read(tempBuffer, binary.LittleEndian, &info.Model)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Sprite)
				binary.Read(tempBuffer, binary.LittleEndian, &info.W)
				binary.Read(tempBuffer, binary.LittleEndian, &info.H)
				binary.Read(tempBuffer, binary.LittleEndian, &info.NumRemaps)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Passengers)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Wreck)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Rating)
				binary.Read(tempBuffer, binary.LittleEndian, &info.FrontWheelOffset)
				binary.Read(tempBuffer, binary.LittleEndian, &info.RearWheelOffset)
				binary.Read(tempBuffer, binary.LittleEndian, &info.FrontWindowOffset)
				binary.Read(tempBuffer, binary.LittleEndian, &info.RearWindowOffset)
				binary.Read(tempBuffer, binary.LittleEndian, &info.InfoFlags)
				binary.Read(tempBuffer, binary.LittleEndian, &info.InfoFlags2)
				info.Remap = make([]uint8, info.NumRemaps)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Remap)
				binary.Read(tempBuffer, binary.LittleEndian, &info.NumDoors)
				info.Doors = make([]DoorInfo, info.NumDoors)
				binary.Read(tempBuffer, binary.LittleEndian, &info.Doors)
				cars = append(cars, info)
			}
			newChunk.Data = cars

		case "SPEC", "FONB", "RECY", "PSXT":
			buffer.Next(newChunk.Size)

		default:
			fmt.Println("Unknown style chunk: ", newChunk.Name)
			buffer.Next(newChunk.Size)
		}

		// Add chunk to the list
		newSTY.Chunks[newChunk.Name] = newChunk
	}

	return newSTY
}
