### GTA 2 Style and Map File Loader ###

* For documentation see [Godoc] (https://godoc.org/gitlab.com/tapir/gta2)
* Uses SFML but can be removed with minimal effort
* A simple example to draw a map is provided

## Style File (95% complete)
- [x] Palettes
- [x] Tiles
- [x] Sprites
- [x] Delta bitmaps
- [x] Map object info
- [x] Car info
- [ ] Font
- [ ] Recycle info

## Map File (20% complete)
- [x] Compressed map blocks
- [ ] Zones
- [ ] Lights
- [ ] Tile info
- [ ] Junction list
- [ ] Map objects

## Screenshot

![Screenshot](https://gitlab.com/tapir/gta2/raw/master/assets/screenshot.png)