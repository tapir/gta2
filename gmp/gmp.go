package gmp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"
)

type Chunk struct {
	Name string
	Size int
	Data interface{}
}

type GTAMap struct {
	Type    string
	Version int
	Chunks  map[string]Chunk
}

type BlockInfo struct {
	Left      uint16
	Right     uint16
	Top       uint16
	Bottom    uint16
	Lid       uint16
	Arrows    uint8
	SlopeType uint8
}

type CompressedMap struct {
	Base        [256][256]uint32
	ColumnWords uint32
	Column      []uint32
	NumBlocks   uint32
	Block       []BlockInfo
}

// Get slope and ground types
func (b BlockInfo) DecodeSlope() (int, int) {
	var (
		bits    []bool
		ground  int
		slope   int
		encoded = int(b.SlopeType)
	)

	for i := uint(0); i < 8; i++ {
		bits = append(bits, ((encoded>>i)&1) > 0)
	}

	for i := uint(0); i < 2; i++ {
		if bits[i] {
			ground |= 1 << i
		}
	}

	for i := uint(0); i < 6; i++ {
		if bits[i+2] {
			slope |= 1 << i
		}
	}

	return ground, slope
}

// Get block tile info
func (b BlockInfo) DecodeLidTile() (int, int, bool, bool, int) {
	var (
		bits     []bool
		encoded  = int(b.Lid)
		tileNum  int
		light    int
		rotation int
	)

	for i := uint(0); i < 16; i++ {
		bits = append(bits, ((encoded>>i)&1) > 0)
	}

	for i := uint(0); i < 10; i++ {
		if bits[i] {
			tileNum |= 1 << i
		}
	}

	for i := uint(0); i < 2; i++ {
		if bits[i+10] {
			light |= 1 << i
		}
	}

	for i := uint(0); i < 2; i++ {
		if bits[i+14] {
			rotation |= 1 << i
		}
	}

	return tileNum, light, bits[12], bits[13], rotation
}

// Get map blocks
func (g *GTAMap) GetBlock() [][][]BlockInfo {
	var (
		dmap   = g.Chunks["DMAP"].Data.(CompressedMap)
		blocks [][][]BlockInfo
	)

	blocks = make([][][]BlockInfo, 8)
	for i := 0; i < 8; i++ {
		blocks[i] = make([][]BlockInfo, 256)
		for j := 0; j < 256; j++ {
			blocks[i][j] = make([]BlockInfo, 256)
		}
	}

	for y := 0; y < 256; y++ {
		for x := 0; x < 256; x++ {
			colindex := int(dmap.Base[x][y])
			height := int(dmap.Column[colindex] & 0xFF)
			offset := int((dmap.Column[colindex] & 0xFF00) >> 8)
			for z := 0; z < height; z++ {
				if z >= offset {
					blocks[z][y][x] = dmap.Block[int(dmap.Column[colindex+z-offset+1])]
				}
			}
		}
	}
	return blocks
}

func ReadGTAMap(filename string) *GTAMap {
	var (
		newGMP   = new(GTAMap)
		fileType [4]byte
		version  uint16
	)

	newGMP.Chunks = make(map[string]Chunk)

	// Read whole file to the buffer
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("Can't open file: ", filename)
	}

	// Read file info
	buffer := bytes.NewBuffer(file)
	binary.Read(buffer, binary.LittleEndian, &fileType)
	binary.Read(buffer, binary.LittleEndian, &version)
	if string(fileType[:]) != "GBMP" {
		log.Fatal("Illegal map file type: ", string(fileType[:]))
	}

	newGMP.Type = string(fileType[:])
	newGMP.Version = int(version)

	// Read chunks
	for buffer.Len() > 0 {
		var (
			newChunk  Chunk
			chunkName [4]byte
			chunkSize uint32
		)

		// Read chunk name
		binary.Read(buffer, binary.LittleEndian, &chunkName)

		// Read chunk size
		binary.Read(buffer, binary.LittleEndian, &chunkSize)

		newChunk.Name = string(chunkName[:])
		newChunk.Size = int(chunkSize)

		// Decode chunk types
		switch newChunk.Name {
		case "DMAP":
			// Compressed map for PC
			var dmap CompressedMap
			binary.Read(buffer, binary.LittleEndian, &dmap.Base)
			binary.Read(buffer, binary.LittleEndian, &dmap.ColumnWords)
			dmap.Column = make([]uint32, dmap.ColumnWords)
			binary.Read(buffer, binary.LittleEndian, &dmap.Column)
			binary.Read(buffer, binary.LittleEndian, &dmap.NumBlocks)
			dmap.Block = make([]BlockInfo, dmap.NumBlocks)
			binary.Read(buffer, binary.LittleEndian, &dmap.Block)
			newChunk.Data = dmap

		case "CMAP", "UMAP", "PSXM":
			buffer.Next(newChunk.Size)

		default:
			fmt.Println("Unknown map chunk: ", newChunk.Name)
			buffer.Next(newChunk.Size)
		}

		// Add chunk to the list
		newGMP.Chunks[newChunk.Name] = newChunk
	}

	return newGMP
}
